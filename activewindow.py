import win32.win32gui
import time

wgui = win32.win32gui

activeWindow = wgui.GetWindowText(wgui.GetForegroundWindow())
newActiveWindow = ""
start = int((time.time() % 60))
end = ""

print("Active window: ", activeWindow)

while True:
    newActiveWindow = wgui.GetWindowText(wgui.GetForegroundWindow())

    if activeWindow != newActiveWindow and newActiveWindow != "":
        end = int((time.time() % 60))
        print("you've spent ", (end - start), " seconds in the prev. window")
        start = int((time.time() % 60))
        print("New active window: ", newActiveWindow)
        activeWindow = newActiveWindow
