import time
import json
import win32.win32gui as gui

t1 = time.time()
time.sleep(2)
t2 = time.time()
app = gui.GetWindowText(gui.GetForegroundWindow())

# dict in textdatei schreiben
app_dict = {
    "app_name": app,
    "time_spent": (t2-t1)
}

with open('test.txt', 'w') as json_file:
    json.dump(app_dict, json_file)

# textdatei öffnen und json in prettyprint ausgeben
with open('test.txt') as f:
    data = json.load(f)

print(json.dumps(data, indent=4, sort_keys=True))
