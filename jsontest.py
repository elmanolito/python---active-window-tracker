# lib um mit Systemkram interagieren zu können (gui)
import win32.win32gui as gui
# lib um Tastendruck zu erkennen
# from msvcrt import getch

import time
import json


currApp = gui.GetWindowText(gui.GetForegroundWindow())
newApp = ""
t1 = time.time()
t2 = ""
app_dict = ""

while True:
    # key = ord(getch())
    # if key == 27:
    #     break
    newApp = gui.GetWindowText(gui.GetForegroundWindow())

    if currApp != newApp and newApp != "":
        t2 = time.time()
        timeSpent = t2-t1
        app_dict = {
            "date_and_time": time.ctime(),
            "app_name": currApp,
            "time_spent_in_sec": timeSpent
        }

        with open('data.json', 'a', encoding='utf-8') as f:
            json.dump(app_dict, f, ensure_ascii=False, indent=4)
            t1 = time.time()

        currApp = newApp

# Datei wird nicht gespeichert
